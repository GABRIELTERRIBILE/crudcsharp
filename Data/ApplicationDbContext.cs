using GabiCrudTest.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace GabiCrudTest.Data;

public class ApplicationDbContext : IdentityDbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    public DbSet<Student> students = default!;
    public DbSet<Premium> Premiums = default!;
    public DbSet<GabiCrudTest.Models.Student> Student { get; set; } = default!;
    public DbSet<GabiCrudTest.Models.Premium> Premium { get; set; } = default!;
}
