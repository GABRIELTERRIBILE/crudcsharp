using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GabiCrudTest.Models;

public class Premium
{
    [Key]
    [DisplayName("Id")]
    public int Id { get; set; }

    [Required(ErrorMessage = "Informe o titulo do Premium")]
    [StringLength(80, ErrorMessage = "O t�tulo deve conter at� 80 caracteres")]
    [MinLength(5, ErrorMessage = "O t�tulo deve conter pelo menos 5 caracteres")]
    [DisplayName("T�tulo")]
    public string title { get; set; } = string.Empty;

    [DataType(DataType.DateTime)]
    [DisplayName("In�cio")]
    public DateTime StartDate { get; set; }

    [DataType(DataType.DateTime)]
    [DisplayName("T�rmino")]
    public DateTime EndDate { get; set; }

    [DisplayName("Aluno")]
    [Required(ErrorMessage = "Alunos inv�lidos")]
    public int StudentId { get; set; }

    public Student? Student { get; set; }
}