# Projeto ASP.NET Core com CRUD de Alunos e Assinaturas



## Começando

Este projeto é uma aplicação ASP.NET Core que fornece um sistema de gerenciamento para alunos e suas assinaturas. Ele implementa operações CRUD completas (Create, Read, Update e Delete) para ambas as entidades. Além disso, inclui um sistema de autenticação para proteger o acesso aos dados.

## Funcionalidades

1. CRUD de Alunos:

- Cadastrar um novo aluno.
- Listar todos os alunos.
- Atualizar informações de um aluno.
- Excluir um aluno.


2. CRUD de Assinaturas:

- Cadastrar uma nova assinatura para um aluno.
- Listar todas as assinaturas de um aluno específico.
- Atualizar informações de uma assinatura.
- Excluir uma assinatura.

3. Sistema de Autenticação:

- Cadastro de novos usuários.
- Login e Logout.
- Proteção de rotas para acesso apenas por usuários autenticados.


```
cd existing_repo
git remote add origin https://gitlab.com/GABRIELTERRIBILE/crudcsharp.git
git branch -M main
git push -uf origin main
dotnet restore
dotnet run
```
## Ferramentas utilizadas

- .NET SDK
- Visual Studio ou qualquer outro editor de sua preferência.
- SQlite

## Contribuição

Este é um projeto de estudos, Sinta-se à vontade para contribuir com este projeto. Qualquer feedback, melhoria ou correção é bem-vinda.

